FROM eclipse-temurin:8u402-b06-jre-alpine

WORKDIR /app

ADD ["sbrw-mp.jar", "/app/"]

CMD ["java", "-jar", "sbrw-mp.jar"]
